﻿using System;
using UnityEngine;

public class TimingPeriphery : MonoBehaviour
{
    public SpriteRenderer[] texte = new SpriteRenderer[8];

    protected System.Random rnd = null;

    private int[] marcat = new int[8] { 0, 0, 0, 0, 0, 0, 0, 0 };

    private long[] timpi = new long[8]{160000, 320000, 400000, 640000, 720000, 960000, 1100000, 11800000};

    private int index_curent = 0;

    private int pos = -1;

    private long timp_afisare = 0;

    private DateTime now = DateTime.Now;

    private bool we_are_waiting = false;

    private System.Diagnostics.Stopwatch answerFast = new System.Diagnostics.Stopwatch();

    // Start is called before the first frame update
    void Start()
    {
        now = DateTime.Now;
        Debug.Log("[" + now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "] Am pornit");
        answerFast.Start();
        rnd = new System.Random();
        disable_enable_just_one();
        //t1.enabled = t2.enabled = t3.enabled = t4.enabled = false;
    }

    private void disable_enable_just_one(int index_enabled = -1)
    {
        for (int i = 0; i < texte.Length; i++)
            texte[i].enabled = false;
        if (index_enabled != -1)
        {
            texte[index_enabled].enabled = true;
            marcat[index_enabled] = 1;
        }
            
    }

    // Update is called once per frame
    void Update()
    {
        if (answerFast.ElapsedMilliseconds >= 1283000)
        {
            now = DateTime.Now;
            Debug.Log("[" + now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "] Am oprit aplicatia!");
            answerFast.Stop();
            answerFast.Reset();
            Application.Quit();
        }
        if (answerFast.ElapsedMilliseconds >= timpi[index_curent] && we_are_waiting == false) // 20 secunde
        {
            we_are_waiting = true;
            do
            {
                pos = rnd.Next(0, 8);
            } while (marcat[pos] != 0);
            
            //if (marcat[pos] != 0)
               // return; // pozitia a fost deja ocupata, textul deja afisat
            
            timp_afisare = pos <= 3 ? 8000 : 16000;
            disable_enable_just_one(pos);
            now = DateTime.Now;
            Debug.Log("[" + now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "] Am activat textul " + texte[pos].name + " la secunda " + now.Second.ToString() + " ( index_curent=" + index_curent.ToString() + ")");
        }
        if (answerFast.ElapsedMilliseconds >= timpi[index_curent]+timp_afisare && we_are_waiting && marcat[pos]==1)
        {
            disable_enable_just_one();
            we_are_waiting = false;
            now = DateTime.Now;
            Debug.Log("[" + now.ToString("MM/dd/yyyy hh:mm:ss.fff") + "] Am dezactivat textul " + texte[pos].name + " la secunda " + now.Second.ToString() + " ( index_curent="+ index_curent.ToString() + 
                 ", w_are_waiting=" + we_are_waiting.ToString() + ", isRunning=" + answerFast.IsRunning.ToString() + ")");
            marcat[pos] = 2;
            if (index_curent < 8)
                index_curent++;
        }
    }
}
